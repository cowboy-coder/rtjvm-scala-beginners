package lectures.part1basics

object Functions extends App {

  def aFunction(a: String, b: Int): String = {
    a + " " + b
  }

  println(aFunction("hello", 3))

  def aParameterlessFunction(): Int = 42
  println(aParameterlessFunction())

  def aRepeatedFunction(aString: String, n: Int): String = {
    if (n == 1) aString
    else aString + aRepeatedFunction(aString, n - 1)
  }

  println(aRepeatedFunction("hello", 3))

  // WHEN YOU NEED LOOPS, USE RECURSION.

  def aFunctionWithSideEffect(aString: String): Unit = println(aString)

  def aBigFunction(n: Int): Int =  {
    def aSmallerFunction(a: Int, b: Int): Int = a + b

    aSmallerFunction(n, n-1)
  }

  /*
  1. A greeting function (name, age) = > "Hi, my name is $name and I am $age years old"

  2. Factorial function - computes the product of all numbers up to a give number 1 * 2 * 3 * .. * n

  3. A fibonacci function - return the value of the n'th fibonacci number
    f(1)  = 1
    f(2)  = 1
    f(n)  = f(n - 1) + f(n - 2)

  4. Tests if a number is prime. You will need an auxillary function.
  */

  def greeting(name: String, age: Int): String  = {
    "Hi, my name is " + name + " and I am " + age + " years old."
  }
  println(greeting("Sean", 42))

  def factorial(n: Int): Int = {
    if (n <=0) 1
    else n * factorial(n - 1)
  }

  println(factorial(0))

  def fib(x: Int): Int = {
    if (x <= 2) 1
    else fib(x - 1) + fib(x - 2)
  }
  // 1 1 2 3 5 8 13 21...

  println(fib(1))
  println(fib(2))
  println(fib(3))
  println(fib(4))
  println(fib(5))
  println(fib(6))
  println(fib(7))
  println(fib(8))


  def isPrime(n: Int): Boolean = {

    // Does n have any divisors until the number t.
    def isPrimeUntil(t: Int): Boolean = {
      if (t <= 1) true // n is prime until it reaches one. There are no divisor between 2 and 1
      else n % t != 0 & isPrimeUntil(t-1)
    }

    isPrimeUntil(n / 2)
  }

  println(isPrime(17))


}
