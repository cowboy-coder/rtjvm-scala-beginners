package lectures.part1basics

object StringOps extends App {
  val str: String = "Hello, I am learning Scala"

  println(str.charAt(2))
  println(str.substring(7, 11))
  println(str.split(" ").toList)
  println(str.startsWith("Hello"))
  println(str.replace(" ", "-"))
  println(str.toLowerCase())
  println(str.length)

  val aNumberString = "2"
  val aNumber = aNumberString.toInt
  println('a' +: aNumberString :+ 'z')
  println('a' +: aNumberString :+ 'z')
  println(str.reverse)

  // Removes characters from string
  println(str.take(2))

  // Scala-specific: String interpolators

  // S-interpolators
  val name = "Sean"
  val age = 12
  val greeting = s"Hello, my name is $name and I am $age years old"
  println(greeting)

  // s interpolated string and evaluate complex expression
  val anotherGreeting = s"Hello, my name is $name and I will be turning ${age + 1}"
  println(anotherGreeting)

  // F-Interpolators
  // Can evaluate value and complex expressions in strings and can perform printf format
  val speed = 1.2f
  val myth = f"$name%s can eat $speed%2.2f burgers per minute"
  println(myth)

  // raw-interpolators - has the ability to print characters literally

  println(raw"This is a \n newline")
  val escaped = "This is a \n newline"
  println(raw"$escaped")


}
