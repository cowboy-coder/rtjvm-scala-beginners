package lectures.part1basics

object CVNvsCBV extends App {

  def calledByValue(x: Long): Unit = {
    println("by value: " + x)
    println("by value: " + x)
  }

  // with the "x" argument, the arrow tells the compiler that the parameter will be called by name
  // The arrow delays the evaluation of the parameter until it is used in the function.
  def calledByName(x: => Long): Unit = {
    println("by name: " + x)
    println("by name: " + x)
  }

  calledByValue(System.nanoTime()) // the value of System.nanoTime() is calculated prior the the calledByValue function getting called.
  calledByName(System.nanoTime())  // the value of System.nanoTime() is NOT calculated prior the the calledByValue function getting called.

  def infinite(): Int = 1 + infinite()
  def printFirst(x: Int, y: => Int) = println(x) // y is never called, so infinite is never called because it is not actually use the printFirst method

  printFirst(34, infinite())
}
