package lectures.part1basics

object Expressions extends App {
  val x = 1 + 2 // EXPRESSION
  println(x)

  println(2 + 3 * 4)
  // Operators
  // + - * / &  | ^ << >> >>> (right shift with zero extension)

  println(1 == x)
  // == != > >= < <=

  println(!(1 == x)) // logic negation
  // / ! && ||

  var aVariable = 2
  aVariable +=3 // also works with -= *= /= ... these are side effect because we are changing variables.
  println(aVariable)

  // Instructions (DO) vs Expressions

  // IF expression
  val aCondition = true
  val aConditionedValue = if(aCondition) 5 else 3 // IF EXPRESSION - because it gives back a value, it doesn't compute something.
  println(aConditionedValue)
  println(if(aCondition) 5 else 3)

  var i = 0
  while (i < 10) {
    println(i)
    i += 1
  }
  // NEVER WRITE THIS AGAIN (the while loop above) - while loops are imperative instructions, not expressions

  // EVERYTHING in Scala is an Expression!

  val aWeirdValue = (aVariable = 3) // Unit === void
  println(aWeirdValue) // The output value of Unit is () which is the only value it can hold

  // Code Blocks
  val aCodeBlock = {
    val y = 2
    val z = y + 1

    if (z > 2) "hello" else "goodbye"
  }

  // 1. Difference between string "hello world" vs println("hello world")?
        // The string is and expression that return a String.
        // The println is a side effect that returns a Unit.

  // 2. Whats the value of the following:
  //    val someValue = {
  //      2 < 3
  //    }
  //
  //    It should equal True
  //
  //  3. Whats the value of the following:
  //     val someOtherValue = {
  //       if(someValue) 239 else 986
  //       42
  //     }
  //
  //    It should be 42
}
