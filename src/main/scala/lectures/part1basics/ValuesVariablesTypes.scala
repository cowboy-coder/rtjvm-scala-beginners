package lectures.part1basics

object ValuesVariablesTypes extends App {

  val x: Int = 42
  println(42)

  // VALS ARE IMMUTABLE

  // COMPILER can infer types

  // semicolons are only necessary when writing expressions on the same line but this practice is discouraged

  val aString: String = "hello";
  val anotherString = "goodbye"

  val aBoolean: Boolean = false
  val aChar: Char = 'a'
  val anInt: Int = x
  val aShort: Short = 4613 // shorts can be no longer than 4 bytes
  val aLong: Long = 5273985273895237L // longs are 8 bytes
  val aFloat: Float = 2.0f
  val aDouble: Double = 3.14

  var aVariable: Int = 4
  aVariable = 5 // side effect

  //

}
