package lectures.part1basics

import scala.annotation.tailrec
import scala.jdk.Accumulator

object Recursion extends App {
  def factorial(n: Int): Int = {
    if (n <=1 ) 1
    else {
      println("Computing factorial of " + n + " - I first need factorial of  " + (n-1))
      val result = n * factorial(n-1)
      println("Computed factorial of " + n)
      result
    }
  }

  print(factorial(10))
//  print(factorial(5000))

  def anotherFactorial(n: Int): BigInt = {
    @tailrec
    def factHelper(x: Int, accumulator: BigInt): BigInt = {
      if (x<=1) accumulator
      else factHelper(x - 1, x * accumulator) // TAIL RECURSION = use recursive call as the LAST expression
    }
    factHelper(n, 1)
  }

  /*
    anotherFactorial(10) = factHelper(10, 1)
    = factHelper(9, 10 * 1)
    = factHelper(8, 9 * 10 * 1)
    = factHelper(7, 8 * 9 * 10 * 1)
    = ...
    = factHelper(2, 3 * 4 * 5 * 6 * 7 * 8 * 9 * 10)
  */

  println(anotherFactorial(5000))

  // When you need need loops, use _TAIL_ RECURSION

  /*
    1. Concatenate a string n times using tail recursion
    2. IsPrime function tail recursive
    3. Fibonacci function, tail recursive
  */

  @tailrec
  def concatenateTailrec(aString: String, n: Int, accumulator: String): String = {
    if (n <= 0) {
      accumulator
    }
    else {
      concatenateTailrec(aString, n - 1, aString + accumulator)
    }
  }
  println(concatenateTailrec("Hello", 3, ""))

  // Determine if a number is prime or not
  def isPrime(n: Int): Boolean = {

    // Use tail recursion to determine if number is prime
    // t - The divisor you want to check to see if it is divisible by
    @tailrec
    def isPrimeTailRec(quotient: Int, isStillPrime: Boolean): Boolean = {
      println(quotient + " - " + isStillPrime)

      if (!isStillPrime) false  // Return false if isStillPrime is false
      else if (quotient <= 1) true // Return true if the quotient is less than or equal to 1
      else {
        // quotient - 1 - iterate to the next lowest number
        // n % quotient != 0 - If it's not divisible, this will not equal zero making isStillPrime false
        isPrimeTailRec(quotient - 1, n % quotient != 0 && isStillPrime)
      } // Call isPrimeTailRec
    }
    // Start with half the number you wanted to check since that is the limit in which we can determine a number
    // is divisible by
    isPrimeTailRec(n/2, true)
  }

  println(isPrime(629))

  def fibonacci(n: Int): Int = {
    def fiboTailrec(i: Int, last: Int, nextToLast: Int): Int = {
      if (i >= n) last
      else fiboTailrec(i + 1, last + nextToLast, last)
    }

    if (n <= 2) 1
    else fiboTailrec(2, 1, 1)
  }

  println(fibonacci(8))
}
