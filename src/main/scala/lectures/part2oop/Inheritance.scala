package lectures.part2oop

object Inheritance extends App{

  class Animal {
    val creatureType = "wild"
    def eat = println("nomnom")
  }

  final class Cat extends Animal {
    def crunch = {
      eat
      println("crunch")
    }
  }

  val cat = new Cat
  cat.crunch


  // constructors
  class Person(name: String, age: Int) {
    // auxillary constructor - alternative constructors
    def this(name: String) = this(name, 0)
  }
  class Adult(name: String, age: Int, idCard: String) extends Person(name, age)


  // overriding
  class Dog(override val creatureType: String) extends Animal {
    override def eat = {
      super.eat
      println("crunch, crunch")
    }
  }
  val dog = new Dog("K9")
  dog.eat
  println(dog.creatureType)


  // type substitution (broad: polymorphism)
  val unknownAnimal: Animal = new Dog("K9")
  unknownAnimal.eat

  // super
  // use this to call the parent class method you overrode

  // Preventing overrides
  // 1 - use final on member
  // 2 - use final on the entire class
  // 3 - seal the class

}
