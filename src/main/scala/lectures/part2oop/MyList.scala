package lectures.part2oop

abstract class MyList {

  /**
   * Singley linked listed
   * header = first element of the list
   * tail = remainder of list
   * isEmpty = is this list empty
   * add(int) => new list with in element added
   * toString => a string represention of the list
   *
   * The list is immutable.  When we return a new list with "add" it'll be a new list.
   */

  def head: Int
  def tail: MyList
  def isEmpty(): Boolean
  def add(element: Int): MyList
  def printElements: String
  // polymorphic call
  override def toString: String = "[" + printElements + "]"
}

object Empty extends MyList {
  def head: Int = throw new NoSuchElementException()
  def tail: MyList = throw new NoSuchElementException()
  def isEmpty(): Boolean = true
  def add(element: Int): MyList = new Cons(element, Empty)
  def printElements: String = ""
}

class Cons(h: Int, t: MyList) extends MyList {
  def head: Int = h
  def tail: MyList = t
  def isEmpty(): Boolean = false
  def add(element: Int): MyList = new Cons(element, this)

  def printElements: String = {
    if(t.isEmpty()) s"${h}"
    else s"${h} ${t.printElements}"
  }
}

trait MyPredicate[-T] {
  def test(elem: T): Boolean
}

object ListTest extends App {
  val list = new Cons(1, new Cons(2, new Cons(3, Empty)))
  println(list.tail.head)
  println(list.add(4).head)
  println(list.isEmpty())
  println(list.toString)
}