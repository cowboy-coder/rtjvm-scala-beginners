package lectures.part2oop

import java.util.Date

object OOBasics extends App {
  val person = new Person("John", 26)
  println(person)
  println(person.age)
  println(person.x)
  person.greet("Sean")

  val author = new Writer("Charlies", "Dickens", 1812)
  val novel = new Novel("Great Expectations", 1861, author)
  println(novel.authorAge())
  println(novel.isWrittenBy(author))

  val counter = new Counter()
  counter.inc.print

  counter.inc.inc.inc.print

  counter.inc(10).print
}

// constructor
class Person(name: String, val age: Int) {
  // body - defines the implementation of the class
  val x = 2
  println(1 + 3)

  // method
  def greet(name: String): Unit = println(s"${this.name} says: Hi, $name")

  // overloading
  def greet(): Unit = println(s"Hi, I am $name")


  def this(name: String) = this(name, 0)
  def this() = this("John Doe")
}

/*
  Implement a Novel and Writer Class

  Writer should have firstName, surName, year of birth
    - fullname - concat first and sur name

  Novel should have name, yearOfRelease, and author
    - authorAge - age of author at year of release
    - isWrittenBy(author)
    - copy (new year of release) = new instance of Novel


  Counter class
  - Receives and int value
  - method current count
  - method to increment/decrement by 1 which returns a new counter
  - overload inc/dec to receive an amount


*/

class Writer(val firstName: String, val surName: String, val year: Int) {
  def fullName(): String = {
    s"${this.firstName} ${this.surName}"
  }
}

class Novel(val name: String, yearOfRelease: Int, val author: Writer) {

  def authorAge() = yearOfRelease - this.author.year

  def isWrittenBy(author: Writer) = this.author == author

  def copy(newYearOfRelease: Int): Novel = {
    Novel(this.name, newYearOfRelease, this.author)
  }
}

class Counter(val count: Int = 0) {
  def inc = {
    println("incrementing")
    new Counter(count + 1) // immutability
  }

  def dec = {
    println("decrementing")
    new Counter(count - 1)  // immutability
  }

  def inc(n: Int): Counter = {
    if (n <= 0) this
    else inc.inc(n-1)
  }

  def dec(n: Int): Counter = {
    if (n <= 0) this
    else dec.dec(n - 1)
  }

  def print = println(count)
}