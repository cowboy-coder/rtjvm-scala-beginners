package lectures.part2oop

object AbstractDataTypes extends App {

  // abstract
  abstract class Animal {
    val creatureType: String = "wild"
    def eat: Unit // unimplemented so it's abstract
  }

  class Dog extends Animal {
    override val creatureType: String = "Canine" // override is not necessary if the member was abstract
    def eat: Unit = println("crunch crunch")
  }

  trait Carnivore {
    def eat(animal: Animal): Unit
    val preferredMeal: String = "fresh meat"
  }

  trait ColdBlooded

  // Here we are extending a class with a trait
  class Crocodile extends Animal with Carnivore with ColdBlooded {
    // inherits members from both Animal and Carnivore
    override val creatureType: String = "croc"
    def eat: Unit = println("nomnomnom")
    def eat(animal: Animal): Unit = println(s"I'm a croc and I'm eating ${animal.creatureType}")
  }

  val dog: Dog = new Dog
  val croc: Crocodile = new Crocodile
  croc.eat(dog)

  // traits vs abstract classes
  // 1.
}
