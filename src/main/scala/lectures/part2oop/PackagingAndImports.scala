package lectures.part2oop

class PackagingAndImports {

  // package members are accessible by their simple name
  val write = new Writer("Daniel", "RocketTheJVM", 2018)

  // package object
  // originated from the problem that sometime we want to write methods or constants outside of everything else
  sayHello
  println(SPEED_OF_LIGHT)
}
