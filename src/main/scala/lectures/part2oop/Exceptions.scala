package lectures.part2oop

object Exceptions extends App {

  val x: String = null

  // This will crash with a NPE
  //  println(x.length)

  // 1. Throwing and Catching Exceptions

  // Evaluates to Nothing
  // Nothing is a valid substitute for any other type
  //val aWeirdValue: String = throw new NullPointerException()

  // Throwable classes extend the Throwable class
  // Exception and Error are the major Throwable subtypes

  // 2. How to catch exceptions
  def getInt(withExceptions: Boolean): Int =
    if (withExceptions) throw new RuntimeException("No int for you.")
    else 42

  val potentialFail = try {
    // code that might throw
    getInt(false)
  } catch {
    case e: RuntimeException => println("caught a runtime exception")
  } finally {
    // code that will get executed no matter what
    // optional
    // does not influenc the retur type of this expression
    // use finally only for side effects
    println("finally")
  }

  println(potentialFail)

  // 3. How to define your own exceptions

  class MyException extends Exception
  val exception = new MyException
  throw exception

  /**
   * 1. Crash your program  with a OOM error
   * 2. Crash with StackOverflow Error
   * 3. PocketCalculator
   *    - add(x, y)
   *    - subtract(x, y)
   *    - multiply(x, y)
   *    - divide(x, y)
   *
   *    Throw
   *      - OverflowException if add(x,y) exceeds Int.MAX_VALUE
   *      - UnderflowException if subtract(x,y) exceeds Int.MIN_VALUE
   *      - MathCaclulationException for division by 0
   */

  //  OOM error
  //val array = Array.ofDim(Int.MaxValue)

  // SO
  //def infinite: Int = 1 + infinite
  //val noLimit = infinite
  class OverflowException extends RuntimeException
  class UnderflowException extends RuntimeException
  class MathCalculationException extends RuntimeException("Division by 0")

  object PocketCalculator {

    def add(x: Int, y: Int) = {
      val result = x + y
      if (x > 0 && y > 0 && result < 0) throw new OverflowException
      else if (x < 0 && y > 0 && result > 0) throw new UnderflowException
      else result
    }

    def subtract (x: Int, y: Int) = {
      val result = x - y
      if (x> 0 && y < 0 && result < 0) throw new OverflowException
      else if (x < 0 && y > 0 && result > 0) throw new UnderflowException
      else result
    }

    def multiply(x: Int, y: Int) = {
      val result = x * y
      if (x > 0 && y > 0 && result < 0) throw new OverflowException
      else if (x < 0 && y < 0 && result < 0) throw new OverflowException
      else if (x > 0 && y < 0 && result < 0) throw new UnderflowException
      else if (x < 0 && y > 0 && result > 0) throw new UnderflowException
    }

    def divide(x: Int, y: Int) = {
      val result = x * y
      if (y == 0) throw new MathCalculationException
      else x / y
    }
  }



  println(PocketCalculator.add(Int.MaxValue, 10))
//  println(PocketCalculator.divide(2, 0))
}
