package lectures.part2oop

object CaseClasses extends App {

  case class Person(name: String, age: Int)

  // 1. class parameters are fields
  val jim = new Person("Jim", 34)
  println(jim)

  // 3. equals and hashcode implemented out of the box
  val jim2 = new Person("Jim", 34)
  println(jim == jim2)

  // 4. Case classes have handy copy methods
  val jim3 = jim.copy(age = 45)
  println(jim3)

  // 5. Case classes have companion objects
  val thePerson = Person
  val mary = Person("Mary", 23)

  // 6. Case classes are serializable
  // Akka deals with send seriablizable methods through the network

  // 7. Case class have extractor patterns - can be used in pattern matching.

  case object UnitedKingdom {
    def name: String = "The UK of GB and NI"
  }


}
