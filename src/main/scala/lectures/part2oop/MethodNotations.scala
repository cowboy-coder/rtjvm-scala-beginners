package lectures.part2oop

import scala.language.postfixOps

object MethodNotations extends App {
  class Person(val name: String, favoriteMovie: String, val age: Int) {
    def likes(movie: String): Boolean = movie == favoriteMovie
    def +(person: Person): String = s"${this.name} is hanging out with ${person.name}"
    def unary_! : String = s"$name, what the heck?!" // The space is important before the colon because if you don't put in a space the compiler will thing the colon is part of the method name.
    def isAlive: Boolean = true
    def apply(): String = s"Hi, my name is $name and I like $favoriteMovie" // parens are important
    def apply(count: Int): String = s"${this.name} watched the movie ${favoriteMovie} ${count} times." // parens are important

    def +(nickname: String): Person = new Person(s"${this.name} (${nickname})", this.favoriteMovie, this.age)

    def learns(subject: String): String = s"$this.name is learning $subject"
    def learnsScala: String = this learns "scala"

    def unary_+ : Person = new Person(name, favoriteMovie, age + 1)
  }

  // Infix Notation - syntactic sugar (nicer ways of writing code that cumbersome ways of writing the code)

  val mary = new Person("Mary", "Inception", 23)
  println(mary.likes("Inception"))
  println(mary likes "Inception")  // equivalent - very natural language
  // Above is called infix notation (aka operator notation)
  // - **Only works with method that have 1 parameter**

  // "operators" in Scala
  // - the term operator is used here because "hangOutWith works as an operator between Mary and Tom
  val tom = new Person("Tom", "Fight Club", 23)

  // these two lines are equivalent
  println(mary + tom)
  println(mary.+(tom))

  println(1 + 2)
  println(1.+(2))

  // ALL OPERATORS ARE METHODS

  //----------------------------------------------------------
  // Prefix Notation
  // - all about unary operators

  val x = -1 // equivalent with 1.uanry_-
  val y = 1.unary_-
  // unary_ prefix only works with - + ~ !

  println(!mary)
  println(mary.unary_!)

  //----------------------------------------------------------
  // Postfix Notation - rarely used in practice.
  // Method that have no parameters have the property that they CAN be use in a postfix notation.
  println(mary.isAlive)
  println(mary isAlive)

  // apply
  println(mary.apply())
  println(mary())  // we see a class being called with parameters, it looks for the apply method.
  mary

  /*
  * 1. Overload the + operator --> it should return a new person with a nickname
  * 2. Add an age to the Person clss
       Add a unary + operator => new person with the age + 1
    3. Add a "learns" method in the Person class => "Mary learns Scala"
       Add a learnsScala method, calls learns method with a "Scala".
       Use it in postfix notation.
    3. Overload the apply method
       mary.apply(2) => Mary watched Inception 2 times
  * */
  println((mary + "the rockstar")()) // this is calling the apply method since the + function returns a Person
  println((+mary).age)
  println(mary learnsScala)
  println(mary(10))
}
