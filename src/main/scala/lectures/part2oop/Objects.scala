package lectures.part2oop

object Objects extends App {

  // SCALA DOES NOT HAVE CLASS LEVEL FUNCTIONALITY (IE "static")
  // Objects don't receive parameters and you can access them in a class level setting
  object Person { // the object Person is it's own type and instance - it's a singleton!
    val N_EYES = 2

    def canFly: Boolean = false

    def apply(mother: Person, father: Person): Person = new Person("Bobbie")
  }

  class Person(val name: String) {
  }

  // class Person and object Person are companions because they are in the same
  // scope with the same name.

  println(Person.N_EYES)
  println(Person.canFly)

  // Scala object is a singleton instance
  val mary = new Person("Mary")
  val john = new Person("John")
  println(mary == john) // false because we used the new keyword

  val person1 = Person
  val person2 = Person
  println(person1 == person2) // true because we are using the singleton instance (no new keywork)

  val bobbie = Person(mary, john)

  // Scala Applications
  
}